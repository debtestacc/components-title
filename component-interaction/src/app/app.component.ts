import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'title form app-component';
  
  constructor(public data:DataService){}
  ngOnInit(){
    this.title = <string>this.data.pageTitle;
  }
}
