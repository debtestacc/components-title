import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
pageTitle='data service';

  constructor() { }

  setTitle(title){
    this.pageTitle = title;
  }

  getTitle(){
    return this.pageTitle;
  }
}
