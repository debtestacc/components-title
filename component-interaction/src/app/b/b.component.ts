import { Component, OnInit, OnChanges } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.css']
})
export class BComponent implements OnInit {

  title = "title from Component B";

  constructor(public data: DataService) {}

  ngOnInit() {
    this.data.setTitle(this.title);
  }

 
}
