import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.css']
})
export class AComponent implements OnInit {
  title = "title from Component A";
  constructor(public data: DataService) { }

  ngOnInit() {
    this.data.setTitle(this.title);
  }

}
